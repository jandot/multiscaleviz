var svg = d3.select("body").append("svg").attr({width: 5000, height: 1000})

function flattenData(d, binSize) {
    var slices = d.map(function(x,i) { return d.slice(i-binSize,i+binSize)})
    var newvalues = slices.map(function(x) { return x.reduce(function(a,b) { return a+b }, 0)})
    var normalizedValues = newvalues.map(function(x) { return x/(binSize*2/1)})
    return normalizedValues
}

function draw(d, level, y) {
    svg.selectAll("a" + level.toString() + ".rect")
        .data(flattenData(d, level))
        .enter()
        .append("rect")
        .attr({
            x: function(d,i) { return i+20 },
            y: y,
            width: level,
            height: 1,
//                fill: function(d) { return d3.hsl(d*360,1,0.5)},
            fill: "black",
            opacity: function(d) { return d },
            class: "a" + level.toString()
        })
}

d3.csv("conservation_part.csv", function(d) {
    var values = d.map(function(x) { return x.value }).map(function(x) { return parseFloat(x)} ).filter(function(x) { return x > 0}).map(function(x) { return x/4.187 })
    svg.selectAll("circle")
        .data(values)
        .enter()
        .append("circle")
        .attr({
            cx: function(d,i) { return i+20},
            cy: function(d) { return 280-d*100},
            r: 3,
            fill: function(d) { if ( d > 0 ) { return "green" } else { return "red" } },
            opacity: 0.5
        })
    svg.selectAll("first.rect")
        .data(values)
        .enter()
        .append("rect")
        .attr({
            x: function(d,i) { return i+20 },
            y: 300,
            width: 1,
            height: 5,
            fill: "black",
            opacity: function(d) { return d},
            class: "first"
        })

    var range = Array.apply(null, Array(50)).map(function (_, i) {return i;});
    range.forEach(function(d,i) {
        draw(values, d+2, 300+(1*d))
    })  
})
