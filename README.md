# MultiScaleViz

Displaying data across scales, like this: ![Multiscale image](https://bitbucket.org/jandot/multiscaleviz/src/66ac69c1b2ca164cac54603855032bd1a712fa94/example.png?at=master)

To recreate lib/main.js, run "grunt" (without options). This will call "coffee -b -c --no-header main.coffee"

License: MIT License (c) Jan Aerts 2014
