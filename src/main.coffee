rectHeight = 3

flattenData = (d, binSize) ->
    d.filter (x,i) -> i > binSize && i < (d.length - binSize)
        .map (x,i) -> d[i-binSize..i+binSize]
        .map (x) -> x.reduce (a,b) ->
            a+b
        ,0
        .map (x) -> x/(binSize*2+1)

draw = (d, level, y) ->
	svg.selectAll("a" + level.toString() + ".rect")
		.data(flattenData(d, level))
		.enter()
		.append("rect")
		.attr({
            x: (d,i) -> i+20,
            y: y,
            width: level,
            height: rectHeight,
            fill: "black",
            opacity: (d) -> d,
            class: "a" + level.toString()
		})

svg = d3.select("body").append("svg").attr({width: 5000, height: 1000})

d3.csv "conservation_part.csv", (d) ->
    values = d.map (x) -> x.value
      .map (x) -> parseFloat(x)
      .filter (x) -> x>0
      .map (x) -> x/4.187
    svg.selectAll("circle")
        .data(values)
        .enter()
        .append("circle")
        .attr({
            cx: (d,i) -> i+20,
            cy: (d) -> 280-d*100,
            r: 3,
            fill: (d) -> if d > 0 then "green" else "red",
            opacity: 0.5
        })
    svg.selectAll("first.rect")
        .data(values)
        .enter()
        .append("rect")
        .attr({
            x: (d,i) -> i+20 ,
            y: 300,
            width: 1,
            height: 5,
            fill: "black",
            opacity: (d) -> d,
            class: "first"
        })

    _.range(50).map (d,i) ->
        draw(values, d+2, 300+(rectHeight*d))